package com.example.noteapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.noteapp.databinding.FragmentEditNoteBinding
import com.example.noteapp.model.Note
import com.example.noteapp.model.NoteFormActions
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import java.time.LocalDateTime

class EditNoteFragment : Fragment() {
    private val viewModel by activityViewModels<MainViewModel>(factoryProducer = { NoteViewModelFactory((requireContext().applicationContext as NotesApplication).repository) })
    private var _binding: FragmentEditNoteBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<EditNoteFragmentArgs>()
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEditNoteBinding.inflate(inflater, container, false)

        binding.noteTitleInput.setText(args.noteTitle)
        binding.noteContentInput.setText(args.noteContent)


        binding.saveNoteButton.setOnClickListener {
            onSave()
            findNavController().navigateUp()
        }

        return binding.root
    }

    private fun onSave() = when (args.action) {
        NoteFormActions.EDIT -> {
            binding.noteTitleInput.isEnabled = false
            viewModel.editNote(binding.noteContentInput.text.toString())
        }

        NoteFormActions.ADD -> {
            firebaseAnalytics.logEvent("add_note") {
                param("new_note_added", "true")
            }

            binding.noteTitleInput.isEnabled = true
            viewModel.addNewNote(
                Note(
                    title = binding.noteTitleInput.text.toString(),
                    content = binding.noteContentInput.text.toString(),
                    lastUpdateTime = LocalDateTime.now().toString()
                )
            )
        }
    }
}