package com.example.noteapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.noteapp.databinding.NoteListItemBinding
import com.example.noteapp.model.Note

class NotesAdapter(
    private var notes: MutableList<Note>,
    private val onItemClickCallback: (Int) -> Unit
) : RecyclerView.Adapter<NotesAdapter.NoteViewHolder>() {
    class NoteViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = NoteListItemBinding.bind(view)
        val root get() = binding.root
        val noteTitleView get() = binding.noteTitleView
        val noteDateView get() = binding.noteDateView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.note_list_item, parent, false)
        return NoteViewHolder(view)
    }

    override fun getItemCount(): Int = notes.size

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val note = notes[position]
        holder.noteTitleView.text = note.title
        holder.noteDateView.text = note.lastUpdateTime
        holder.root.setOnClickListener {
            onItemClickCallback(note.id)
        }
    }

    fun setNotes(newNotes: List<Note>) {
        val diffUtilCallback = NoteDiffUtilCallback(this.notes, newNotes)
        val diffNotes = DiffUtil.calculateDiff(diffUtilCallback)
        this.notes.clear()
        this.notes.addAll(newNotes)
        diffNotes.dispatchUpdatesTo(this)
    }
}