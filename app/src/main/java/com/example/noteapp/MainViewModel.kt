package com.example.noteapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.noteapp.model.Note
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(private val repository: NotesRepository) : ViewModel() {
    val allNotes: LiveData<List<Note>> by lazy {
        repository.allNotes.asLiveData()
    }

    private val _note = MutableLiveData<Note>()
    val note: LiveData<Note> = _note

    fun getNoteDetails(noteId: Int) {
        performNoteOperation(
            GetNote(noteId)
        )
    }

    fun addNewNote(note: Note) {
        performNoteOperation(
            AddNewNote(note)
        )
    }

    fun deleteNote(note: Note) {
        performNoteOperation(
            DeleteNote(note)
        )
    }

    fun editNote(newContent: String) {
        performNoteOperation(
            EditNote(newContent)
        )
    }

    private fun performNoteOperation(noteOperation: NoteOperation) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                when (noteOperation) {
                    is AddNewNote -> {
                        repository.addNewNote(noteOperation.note)
                    }
                    is DeleteNote -> {
                        repository.deleteNote(noteOperation.note)
                    }
                    is EditNote -> {
                        note.value?.let {
                            val note = it.copy(content = noteOperation.newContent)
                            repository.updateNote(note)
                        }
                    }
                    is GetNote -> {
                        repository.getNoteDetails(noteOperation.noteId).collect {
                            _note.postValue(it)
                        }
                    }
                }
            }
        }
    }

}

class NoteViewModelFactory(private val repository: NotesRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return MainViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown view model")
    }
}