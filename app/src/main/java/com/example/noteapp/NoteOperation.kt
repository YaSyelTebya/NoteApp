package com.example.noteapp

import com.example.noteapp.model.Note

sealed class NoteOperation

data class DeleteNote(val note: Note): NoteOperation()

data class AddNewNote(val note: Note): NoteOperation()

data class EditNote(val newContent: String): NoteOperation()

data class GetNote(val noteId: Int): NoteOperation()