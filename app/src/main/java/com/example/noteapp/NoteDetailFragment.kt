package com.example.noteapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.noteapp.databinding.FragmentNoteDetailBinding
import com.example.noteapp.model.NoteFormActions
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase

class NoteDetailFragment : Fragment() {
    private val viewModel by activityViewModels<MainViewModel>(factoryProducer = { NoteViewModelFactory((requireContext().applicationContext as NotesApplication).repository) })
    private var _binding: FragmentNoteDetailBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<NoteDetailFragmentArgs>()
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNoteDetailBinding.inflate(inflater, container, false)

        viewModel.note.observe(viewLifecycleOwner) {note ->
            binding.noteTitleView.text = note.title
            binding.noteContentView.text = note.content

            binding.deleteNoteButton.setOnClickListener {
                viewModel.deleteNote(note)
                firebaseAnalytics.logEvent("delete_note") {
                    param("note_deleted", "true")
                }
                findNavController().navigateUp()
            }
            binding.editNoteButton.setOnClickListener {
                val navAction: NavDirections = NoteDetailFragmentDirections
                    .actionNoteDetailFragmentToEditNoteFragment(
                        note.title,
                        note.content,
                        NoteFormActions.EDIT
                    )
                findNavController().navigate(navAction)
            }
        }
        viewModel.getNoteDetails(args.noteId)

        return binding.root
    }
}