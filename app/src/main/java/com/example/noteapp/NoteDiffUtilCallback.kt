package com.example.noteapp

import androidx.recyclerview.widget.DiffUtil
import com.example.noteapp.model.Note

class NoteDiffUtilCallback(private val oldNotes: List<Note>, private val newNotes: List<Note>) : DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldNotes.size

    override fun getNewListSize(): Int = newNotes.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val old = oldNotes[oldItemPosition]
        val new = newNotes[newItemPosition]
        return old.id == new.id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val old = oldNotes[oldItemPosition]
        val new = newNotes[newItemPosition]
        return old.content == new.content && old.lastUpdateTime == new.lastUpdateTime
    }

}
