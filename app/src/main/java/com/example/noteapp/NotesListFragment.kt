package com.example.noteapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.noteapp.databinding.FragmentNotesListBinding
import com.example.noteapp.model.NoteFormActions
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase

class NotesListFragment : Fragment() {
    private val viewModel by activityViewModels<MainViewModel>(factoryProducer = { NoteViewModelFactory((requireContext().applicationContext as NotesApplication).repository) })
    private var _binding: FragmentNotesListBinding? = null
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNotesListBinding.inflate(inflater, container, false)

        binding.notesList.layoutManager = LinearLayoutManager(context)
        binding.notesList.adapter = NotesAdapter(mutableListOf(), this::navigateToDetails)

        viewModel.allNotes.observe(viewLifecycleOwner) {
            (binding.notesList.adapter as NotesAdapter).setNotes(it)
        }

        binding.addNewNoteButton.setOnClickListener {
            val navAction: NavDirections = NotesListFragmentDirections
                .actionNotesListFragmentToEditNoteFragment(
                    NEW_NOTE_DEFAULT_TITLE,
                    NEW_NOTE_DEFAULT_CONTENT,
                    NoteFormActions.ADD
                )
            findNavController().navigate(navAction)
        }

        return binding.root
    }

    private fun navigateToDetails(noteId: Int) {
        Log.d(TAG, "navigateToDetails: analytics must happern")
        firebaseAnalytics.logEvent("view_note") {
            param("note_opened", "true")
        }
        val navAction: NavDirections = NotesListFragmentDirections
            .actionNotesListFragmentToNoteDetailFragment(noteId)
        findNavController().navigate(navAction)
    }

    private companion object {
        const val TAG = "NoteAppNoteListFragment"
        const val NEW_NOTE_DEFAULT_TITLE = ""
        const val NEW_NOTE_DEFAULT_CONTENT = ""
    }
}